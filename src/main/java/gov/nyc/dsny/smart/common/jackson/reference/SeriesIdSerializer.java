package gov.nyc.dsny.smart.common.jackson.reference;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.lang.reflect.Type;

public class SeriesIdSerializer extends StdSerializer<SeriesId> {

	public SeriesIdSerializer() {
		super(SeriesId.class);
	}

	@Override
	public JsonNode getSchema(SerializerProvider serializerProvider, Type type) throws JsonMappingException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void serialize(SeriesId objectId, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
		jsonGenerator.writeString(objectId.getCorrelation());
	}
}