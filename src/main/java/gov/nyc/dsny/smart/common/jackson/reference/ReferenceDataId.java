package gov.nyc.dsny.smart.common.jackson.reference;

import gov.nyc.dsny.smart.common.jackson.aggregate.AggregateId;
import lombok.*;
import org.hibernate.validator.constraints.NotBlank;

/**
 * Created by vgellerman on 2/10/2016.
 */
@RequiredArgsConstructor
@Getter
@EqualsAndHashCode(callSuper = false)
@ToString(callSuper = false)
public class ReferenceDataId extends AggregateId {

	private static final long serialVersionUID = -925210535511442453L;

	@NotBlank
	@NonNull
	protected final String correlation;

}
