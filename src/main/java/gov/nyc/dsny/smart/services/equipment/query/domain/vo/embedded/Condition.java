package gov.nyc.dsny.smart.services.equipment.query.domain.vo.embedded;

import gov.nyc.dsny.smart.common.jackson.reference.DownCodeId;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.data.annotation.AccessType;
import org.springframework.data.annotation.AccessType.Type;
import org.springframework.data.annotation.PersistenceConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * Condition value object.
 */
@Value
@AccessType(Type.FIELD)
@AllArgsConstructor (onConstructor=@__(@PersistenceConstructor))
public class Condition implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8591847231953351694L;

	private String comments;
	private DownCodeId downCodeId;
	private String reporter;
	private String mechanic;
	private String repairLocation;
	private LocalDateTime userEnteredTimestamp;
	private boolean fixed;

}
