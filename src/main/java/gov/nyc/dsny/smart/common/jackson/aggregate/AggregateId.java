package gov.nyc.dsny.smart.common.jackson.aggregate;

import java.io.Serializable;

/**
 * Created by vgellerman on 2/10/2016.
 */
public abstract class AggregateId implements Serializable {

	private static final long serialVersionUID = -925210535511442453L;

	@Override
	public abstract String toString();

	@Override
	public abstract int hashCode();

	@Override
	public abstract boolean equals(Object obj);
}
