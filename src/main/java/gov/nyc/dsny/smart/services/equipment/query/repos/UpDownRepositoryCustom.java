package gov.nyc.dsny.smart.services.equipment.query.repos;

import gov.nyc.dsny.smart.common.jackson.aggregate.EquipmentId;
import gov.nyc.dsny.smart.services.equipment.query.domain.vo.UpDown;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

/**
 * Interface defining custom Equipment UpDown Repository methods.
 */
public interface UpDownRepositoryCustom {

//	Page<UpDown> findByEquipmentId(EquipmentId equipmentId, LocalDateTime date, Pageable pageable);
	List<UpDown> findByEquipmentIdIn(Set<EquipmentId> equipmentIds, LocalDateTime date);
	Page<UpDown> findEquipmentUpDownById(EquipmentId equipmentId, LocalDateTime date, Pageable page);
}
