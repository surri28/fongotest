package gov.nyc.dsny.smart.services.equipment.query.repos;

import com.github.fakemongo.Fongo;
import com.mongodb.Mongo;
import gov.nyc.dsny.smart.common.jackson.SMARTObjectMapper;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Created by ipapkov on 3/9/2016.
 */

@Configuration
@EnableMongoRepositories(basePackages = "gov.nyc.dsny.smart.services.equipment.query.repos")
public class MongoConfig extends AbstractMongoConfiguration {

    @Override
    protected String getDatabaseName() {
        return "equipment";
    }

    @Override
    @Bean
    public Mongo mongo() {
        return new Fongo("menerva-test-server").getMongo();
    }

    @Bean
    public MongoTemplate mongoTemplate() throws Exception {
        return new MongoTemplate(mongo(), getDatabaseName());
    }

    @Bean
    public SMARTObjectMapper smartObjectMapper()
    {
        return new  SMARTObjectMapper();
    }

}
