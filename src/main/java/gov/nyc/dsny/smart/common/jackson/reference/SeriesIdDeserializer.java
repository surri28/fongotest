package gov.nyc.dsny.smart.common.jackson.reference;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

public class SeriesIdDeserializer extends StdDeserializer<SeriesId> {

	/**
	 *
	 */
	private static final long serialVersionUID = 526624613193648263L;

	public SeriesIdDeserializer() {
		super(SeriesId.class);
	}

	@Override
	public SeriesId deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
		return new SeriesId(jsonParser.getText());
	}
}