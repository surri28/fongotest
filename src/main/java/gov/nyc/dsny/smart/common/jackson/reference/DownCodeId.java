package gov.nyc.dsny.smart.common.jackson.reference;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.springframework.data.annotation.PersistenceConstructor;

@Getter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@JsonSerialize(using = DownCodeIdSerializer.class)
@JsonDeserialize(using = DownCodeIdDeserializer.class)
public final class DownCodeId extends ReferenceDataId {

	/**
	 *
	 */
	private static final long serialVersionUID = 8241974582950129818L;

	@PersistenceConstructor
	@JsonCreator
	public DownCodeId(@JsonProperty("correlation") String correlation) {
		super(correlation);
	}


}