package gov.nyc.dsny.smart.common.jackson.reference;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

public class LocationIdDeserializer extends StdDeserializer<LocationId> {

	/**
	 *
	 */
	private static final long serialVersionUID = 526624613193648263L;

	public LocationIdDeserializer() {
		super(LocationId.class);
	}

	@Override
	public LocationId deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
		return new LocationId(jsonParser.getText());
	}
}