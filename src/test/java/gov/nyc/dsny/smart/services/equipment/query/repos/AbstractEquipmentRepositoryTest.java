package gov.nyc.dsny.smart.services.equipment.query.repos;

import gov.nyc.dsny.smart.common.jackson.SMARTObjectMapper;
import gov.nyc.dsny.smart.common.jackson.aggregate.EquipmentId;
import gov.nyc.dsny.smart.common.jackson.reference.LocationId;
import gov.nyc.dsny.smart.common.jackson.reference.SeriesId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.data.mongodb.core.MongoTemplate;

/**
 * Created by sramasamy on 4/11/2016.
 */
public class AbstractEquipmentRepositoryTest {

    protected LocationId locationIdQW05 = new LocationId("QW05");
    protected EquipmentId equipmentId26AM_100 = new EquipmentId(new SeriesId("26AM"), "100");
    protected EquipmentId equipmentId03P_001 = new EquipmentId(new SeriesId("03P"), "001");
    protected EquipmentId equipmentId03P_002 = new EquipmentId(new SeriesId("03P"), "002");
    protected EquipmentId equipmentId03P_003 = new EquipmentId(new SeriesId("03P"), "003");

    @Autowired
    protected ApplicationContext applicationContext;

    @Autowired
    protected MongoTemplate mongoTemplate;

    @Autowired
    protected SMARTObjectMapper smartObjectMapper;

}
