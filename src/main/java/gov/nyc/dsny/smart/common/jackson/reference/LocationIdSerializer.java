package gov.nyc.dsny.smart.common.jackson.reference;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.lang.reflect.Type;

public class LocationIdSerializer extends StdSerializer<LocationId> {

	public LocationIdSerializer() {
		super(LocationId.class);
	}

	@Override
	public JsonNode getSchema(SerializerProvider serializerProvider, Type type) throws JsonMappingException {
		throw new UnsupportedOperationException();
	}

	@Override
	public void serialize(LocationId objectId, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
		jsonGenerator.writeString(objectId.getCorrelation());
	}
}