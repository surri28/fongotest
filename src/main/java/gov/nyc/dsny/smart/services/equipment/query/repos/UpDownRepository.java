package gov.nyc.dsny.smart.services.equipment.query.repos;

import gov.nyc.dsny.smart.common.jackson.aggregate.EquipmentId;
import gov.nyc.dsny.smart.services.equipment.query.domain.vo.UpDown;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.mongodb.repository.MongoRepository;

import java.time.LocalDateTime;

/**
 * Equipment UpDownRepository.
 */
public interface UpDownRepository extends MongoRepository<UpDown, String>, UpDownRepositoryCustom {

    Page<UpDown> findByEquipmentIdAndLastModifiedTimestampLessThanEqual(EquipmentId equipmentId, LocalDateTime lastModifiedTimestamp, Pageable pageable);
}
