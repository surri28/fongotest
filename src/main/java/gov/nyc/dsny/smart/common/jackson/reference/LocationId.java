package gov.nyc.dsny.smart.common.jackson.reference;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import gov.nyc.dsny.smart.common.jackson.reference.LocationIdDeserializer;
import gov.nyc.dsny.smart.common.jackson.reference.LocationIdSerializer;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.springframework.data.annotation.PersistenceConstructor;

@Getter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@JsonSerialize(using = LocationIdSerializer.class)
@JsonDeserialize(using = LocationIdDeserializer.class)
public final class LocationId extends ReferenceDataId {

	/**
	 *
	 */
	private static final long serialVersionUID = 8241974582950129818L;

	@PersistenceConstructor
	@JsonCreator
	public LocationId(@JsonProperty("correlation") String correlation) {
		super(correlation);
	}

}