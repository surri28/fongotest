package gov.nyc.dsny.smart.common.jackson.aggregate;

import gov.nyc.dsny.smart.common.jackson.reference.SeriesId;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.springframework.data.annotation.PersistenceConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

@Getter
@RequiredArgsConstructor(onConstructor = @__(@PersistenceConstructor))
@EqualsAndHashCode(callSuper = false)
public final class EquipmentId extends AggregateId{

	private static final long serialVersionUID = -925210535511442453L;

	@NonNull
	@NotNull
	@Valid
	private final SeriesId seriesId;

	@NonNull
	@NotBlank
	private final String vehicleNumber;

	@Override
	public String toString() {
		return seriesId.getCorrelation() + "-" + vehicleNumber;
	}

}