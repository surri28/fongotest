package gov.nyc.dsny.smart.services.equipment.query.repos;

import com.lordofthejars.nosqlunit.mongodb.InMemoryMongoDb;
import com.lordofthejars.nosqlunit.mongodb.MongoDbRule;
import gov.nyc.dsny.smart.common.jackson.aggregate.EquipmentId;
import gov.nyc.dsny.smart.common.jackson.reference.SeriesId;
import gov.nyc.dsny.smart.services.equipment.query.domain.vo.UpDown;
import lombok.extern.slf4j.Slf4j;
import org.junit.Before;
import org.junit.ClassRule;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.io.InputStream;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static com.lordofthejars.nosqlunit.mongodb.MongoDbRule.MongoDbRuleBuilder.newMongoDbRule;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;

/**
 * Created by sramasamy on 4/12/2016.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes={MongoConfig.class})
@Slf4j
public class UpDownRepositoryTest extends AbstractEquipmentRepositoryTest {

    @ClassRule
    public static InMemoryMongoDb inMemoryMongoDb = InMemoryMongoDb.InMemoryMongoRuleBuilder.newInMemoryMongoDbRule().build();

    @Rule
    public MongoDbRule mongoDbRule = newMongoDbRule().defaultSpringMongoDb("equipment");

    private static boolean initialized;

    @Autowired
    private UpDownRepository upDownRepository;

    @Before
    public void initializeData() throws Exception {

        if (initialized)
            return;

        InputStream eqIs = this.getClass().getClassLoader().getResourceAsStream("data/updown.json");

        UpDown[] eq = smartObjectMapper.readValue(eqIs, UpDown[].class);
        mongoTemplate.insert(Arrays.stream(eq).collect(Collectors.toList()), UpDown.class);
        initialized = true;
    }

    @Test
    public void findAllEquipmentTest() {
        List<UpDown> equipmentList = upDownRepository.findAll();
        log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        for(UpDown upDown: equipmentList){
            log.info("EquipmentId {}", upDown.getEquipmentId().getSeriesId().getCorrelation()+"-"+upDown.getEquipmentId().getVehicleNumber());
        }
        log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        log.info("^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^");
        assertThat(equipmentList, hasSize(8));
    }

    @Test
    public void findByEquipmentIdInTest() {

        Set<EquipmentId> equipmentIds = new HashSet<>(Arrays.asList(equipmentId03P_001, new EquipmentId(new SeriesId("25DK"),"301")));

        List<UpDown> equipmentList = upDownRepository.findByEquipmentIdIn(equipmentIds, LocalDateTime.now());

        log.info("########################################################################");
        log.info(equipmentIds.toString()+"  "+ equipmentIds.size());
        log.info("########################################################################");
        log.info("########################################################################");
        log.info("ddddddddddddddddddddddddddddddddddd "+equipmentList.size());
        log.info("ddddddddddddddddddddddddddddddddddd "+equipmentList.toString());
        log.info("########################################################################");
        log.info("########################################################################");
        log.info("########################################################################");
        assertThat(equipmentList, hasSize(2));
    }
}
