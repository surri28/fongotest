package gov.nyc.dsny.smart.services.equipment.query.repos;

import gov.nyc.dsny.smart.common.jackson.aggregate.EquipmentId;
import gov.nyc.dsny.smart.services.equipment.query.domain.vo.UpDown;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.aggregation.Aggregation;
import org.springframework.data.mongodb.core.aggregation.AggregationResults;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Set;

/**
 * Custom implementation of Equipment Repository. Done to provide easy API to Aggregation Framework provided by Spring and Mongo.
 * All basic (findOne, findAll) operations will still use MongoRepository.
 */
@Slf4j
public class UpDownRepositoryImpl implements UpDownRepositoryCustom {

	private MongoTemplate mongoTemplate;

	@Autowired
	public UpDownRepositoryImpl(MongoTemplate mongoTemplate) {
		this.mongoTemplate = mongoTemplate;
	}

	@Override
	public List<UpDown> findByEquipmentIdIn(Set<EquipmentId> equipmentIds, LocalDateTime date) {
		//find updowns for all given equipment prior to date provided
		Aggregation locationByUpDownAggregation = Aggregation
			.newAggregation(
				Aggregation.match(Criteria.where("lastModifiedTimestamp").lt(date).and("equipmentId").in(equipmentIds)),
				Aggregation.sort(Sort.Direction.DESC, "lastModifiedTimestamp")
				, Aggregation.group("equipmentId").first(Aggregation.ROOT).as("upDown")
				, Aggregation.group("upDown")
			)
			.withOptions(Aggregation.newAggregationOptions().allowDiskUse(true).build());

		//equipment objects are empty except ids
		log.info("--------------------------------YYYYYYYYYYYYYYYYYY------------------------------------------------");
		log.info("--------------------------------------------------------------------------------");
		log.info("--------------------------------------------------------------------------------");
		log.info("--------------------------------------------------------------------------------");
		log.info("--------------------------------------------------------------------------------");
		log.info("--------------------------------------------------------------------------------");
		log.info("--------------------------------------------------------------------------------");
		log.info("--------------------------------------------------------------------------------");
		log.info("--------------------------------------------------------------------------------");
		log.info("--------------------------------------------------------------------------------");
		log.info("--------------------------------------------------------------------------------");
		log.info("--------------------------------------------------------------------------------");
		log.info("findByEquipmentIdIn AggregationResults before execution ");
		AggregationResults<UpDown> upDowns = mongoTemplate.aggregate(locationByUpDownAggregation, UpDown.class, UpDown.class);
		log.info("*********************************************************************************");
		log.info("*********************************************************************************");
		log.info("*********************************************************************************");
		log.info("--------------------------------------------------------------------------------");

		log.info("findByEquipmentIdIn AggregationResults isNull= {}", (upDowns ==null));
		if(upDowns!=null) {
			log.info("findByEquipmentIdIn AggregationResults upDowns size= {}", (upDowns.getMappedResults().size()));
		}
		return upDowns.getMappedResults();
	}

	@Override
	public Page<UpDown> findEquipmentUpDownById(EquipmentId equipmentId, LocalDateTime date, Pageable page) {

		Long count = mongoTemplate.getCollection("upDown").count();
		List<UpDown> upDowns = mongoTemplate.find(new Query(Criteria.where("equipmentId").is(equipmentId).and("lastModifiedTimestamp").lte(date)).with(page), UpDown.class);
		return new PageImpl<>(upDowns, page, count);
	}
}
