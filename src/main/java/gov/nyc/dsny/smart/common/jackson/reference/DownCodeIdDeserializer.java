package gov.nyc.dsny.smart.common.jackson.reference;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;

public class DownCodeIdDeserializer extends StdDeserializer<DownCodeId> {

	/**
	 *
	 */
	private static final long serialVersionUID = 526624613193648263L;

	public DownCodeIdDeserializer() {
		super(DownCodeId.class);
	}

	@Override
	public DownCodeId deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
		return new DownCodeId(jsonParser.getText());
	}
}