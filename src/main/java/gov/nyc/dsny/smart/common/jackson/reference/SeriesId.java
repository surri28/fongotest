package gov.nyc.dsny.smart.common.jackson.reference;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;
import org.springframework.data.annotation.PersistenceConstructor;

@Getter
@EqualsAndHashCode(callSuper = true)
@ToString(callSuper = true)
@JsonSerialize(using = SeriesIdSerializer.class)
@JsonDeserialize(using = SeriesIdDeserializer.class)
public final class SeriesId extends ReferenceDataId {

	private static final long serialVersionUID = 321772125604862280L;

	@PersistenceConstructor
	@JsonCreator
	public SeriesId(@JsonProperty("correlation") String correlation) {
		super(correlation);
	}

}
