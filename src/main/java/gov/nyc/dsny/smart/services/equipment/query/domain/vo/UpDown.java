package gov.nyc.dsny.smart.services.equipment.query.domain.vo;

import gov.nyc.dsny.smart.common.jackson.aggregate.EquipmentId;
import gov.nyc.dsny.smart.services.equipment.query.domain.vo.embedded.Condition;
import lombok.AllArgsConstructor;
import lombok.Value;
import org.springframework.data.annotation.AccessType;
import org.springframework.data.annotation.AccessType.Type;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.PersistenceConstructor;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * UpDown value object.
 */
@Value
@AccessType(Type.FIELD)
@AllArgsConstructor (onConstructor=@__(@PersistenceConstructor))
@Document
public class UpDown implements Serializable {

	/**
	 *
	 */
	private static final long serialVersionUID = 8591847231953351694L;

	@Id
	private String id;
	private EquipmentId equipmentId;
	private Boolean down;
	private LocalDateTime lastModifiedTimestamp;
	private LocalDateTime userEnteredTimestamp;
	private List<Condition> conditions;
}
